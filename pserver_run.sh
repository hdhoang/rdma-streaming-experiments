#!/bin/bash
cur_dir=/home/hdhoang/rdma-streaming-experiments;
# kappa_conf=/home/hdhoang/kappa_conf/kappa_aqua99_01.cfg;
kappa_conf=;
vmstat_on=
# ports=(6800 6801 6802 6803)
# ssl_ports=(6443 6444 6445 6446)
port=6800;
ssl_port=6443;

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/hdhoang/libs/bin:/home/hdhoang/libs/lib:/home/hdhoang/libs/lib64:/home/hdhoang/rdma-streaming-experiments/bin;
node_id=1;
num_procs=10;

# echo $LD_LIBRARY_PATH > /dev/stderr;
#server_opts="$@";
LD_LIBRARY_PATH=$LD_LIBRARY_PATH $cur_dir/bin/pserver \
--private-key=mypkey \
--certificate=mycert \
--threads=$num_procs \
--config=$kappa_conf \
--ssl-port=$ssl_port \
--port=$port \
--node-id=$node_id \
;
