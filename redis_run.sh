redis_bin=~/redis-stable/src/redis-server;
interface=0.0.0.0;
ncores=$(grep -c ^processor /proc/cpuinfo);
nprocs=$ncores;
# nprocs=10;
base_port=6370;

trap "exit" INT TERM ERR;
trap "kill 0" EXIT;

for i in $(seq 1 $nprocs); do
    idx=$(($i - 1));
    let port=$(($base_port + $idx));
    echo "starting redis on /home/hdhoang/redis$idx.sock,$interface:$port";
    # $redis_bin --unixsocket /home/hdhoang/redis$idx.sock --bind $interface --protected-mode no --port $port > /dev/null &;
    $redis_bin /home/hdhoang/rdma-streaming-experiments/redis.conf --protected-mode no --port $port > /dev/null &
    # $redis_bin --bind 0.0.0.0 --protected-mode no --port $port > /dev/null &
done

wait;
