server_if=10.70.0.1;

../httperf/httperf --epoll --hog --timeout=1 \
--wsesslog=1000,0,log-generator/requests_6443.log \
--server=$server_if \
--port=6443 \
--rate=30 \
--ssl \
