#!/bin/bash
CFlags='-std=c++17 -O2 -fno-builtin-malloc -fno-builtin-calloc -fno-builtin-realloc -fno-builtin-free';
cur_dir=$(pwd);
kappa_dir=$cur_dir/rdma_live_streaming/code/kappa;
userver_dir=$cur_dir/userver/src;
pserver_dir=$cur_dir/pserver/;
sdelivery_dir=$cur_dir/shared-delivery;
bin_dir=$cur_dir/bin;

mkdir -p $bin_dir;

make_kappa(){
    echo "Cd into $kappa_dir";
    cd $kappa_dir;
    make -j;
    ret=$?;
    if [ $ret -ne 0 ]; then
        cd $cur_dir;
        echo "${FUNCNAME[0]} FAILED";
        return $ret;
    fi
    
    # cp $kappa_dir/bin/libkappa.so $cur_dir/bin/;
    cp $kappa_dir/bin/producer $cur_dir/bin/;
    cp $kappa_dir/bin/kappa-dissemination $cur_dir/bin/;
    ret=$?;
    if [ $ret -ne 0 ]; then
        cd $cur_dir;
        echo "${FUNCNAME[0]} FAILED";
        return $?;
    fi
    
    cd $cur_dir;
    echo "${FUNCNAME[0]} SUCCESSFUL";
    return 0;
}

make_sdelivery(){
    echo "Cd into $sdelivery_dir";
    cd $sdelivery_dir;
    make -j;
    ret=$?;
    if [ $ret -ne 0 ]; then
        cd $cur_dir;
        echo "${FUNCNAME[0]} FAILED";
        return $ret;
    fi
    
    cp -f $sdelivery_dir/bin/sdelivery $cur_dir/bin/;
    ret=$?;
    if [ $ret -ne 0 ]; then
        cd $cur_dir;
        echo "${FUNCNAME[0]} FAILED";
        return $ret;
    fi
    
    cd $cur_dir;
    echo "${FUNCNAME[0]} SUCCESSFUL";
    return 0;
}

make_userver(){
    echo "Cd into $userver_dir";
    cd $userver_dir;
    echo $(pwd);
    make userver-live -j;
    ret=$?;
    if [ $ret -ne 0 ]; then
        cd $cur_dir;
        echo "${FUNCNAME[0]} FAILED";
        return $ret;
    fi
    
    cp $userver_dir/userver-live $cur_dir/bin/;
    cp $userver_dir/mycert $cur_dir/;
    cp $userver_dir/mypkey $cur_dir/;
    
    cd $cur_dir;
    echo "${FUNCNAME[0]} SUCCESSFUL";
    return 0;
}

make_pserver(){
    echo "Cd into $pserver_dir";
    cd $pserver_dir;
    echo $(pwd);
    make -j;
    ret=$?;
    if [ $ret -ne 0 ]; then
        cd $cur_dir;
        echo "${FUNCNAME[0]} FAILED";
        return $ret;
    fi
    
    cp $pserver_dir/bin/pserver $cur_dir/bin/;
    cp $pserver_dir/mycert $cur_dir/;
    cp $pserver_dir/mypkey $cur_dir/;
    
    cd $cur_dir;
    echo "${FUNCNAME[0]} SUCCESSFUL";
    return 0;
}

make_kappa
ret=$?;
if [ $ret -ne 0 ]; then
    exit $ret;
fi

make_sdelivery;
ret=$?;
if [ $ret -ne 0 ]; then
    exit $ret;
fi

# make_userver;
# ret=$?;
# if [ $ret -ne 0 ]; then
#     exit $ret;
# fi

make_pserver;
