#!/bin/bash
cur_dir=/home/hdhoang/rdma-streaming-experiments;
kappa_conf=/home/hdhoang/kappa_conf/kappa_aqua00_08.cfg;
vmstat_on=
# ports=(6800 6801 6802 6803)
# ssl_ports=(6443 6444 6445 6446)
port=6800
ssl_port=6443

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/hdhoang/libs/bin:/home/hdhoang/libs/lib:/home/hdhoang/libs/lib64:/home/hdhoang/rdma-streaming-experiments/bin;
node_id=1;
num_procs=10;

# echo $LD_LIBRARY_PATH > /dev/stderr;
#server_opts="$@";
server_opts="--dont-exit-on-not-found --max-conns=300 --max-fds=300 --ignore-fd-setsize --use-epoll --reply-buffer-size=256 --read-buffer-size=256";

LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libprofiler.so CPUPROFILE=kappa_tcp.prof \
LD_LIBRARY_PATH=$LD_LIBRARY_PATH \
$cur_dir/bin/userver-live \
--stream-config=$kappa_conf \
--ssl-port=$ssl_port \
--port=$port \
--stream-nodeid=$node_id \
$server_opts;
# --procs=$num_procs \