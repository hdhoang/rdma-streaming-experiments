#!/bin/bash
#  This script is used to configure Linux tc on a client machine.
#  tc uses the following units when passed as a parameter.
#  kbps: Kilobytes per second
#  mbps: Megabytes per second
#  kbit: Kilobits per second
#  mbit: Megabits per second
#  bps: Bytes per second
#       Amounts of data can be specified in:
#       kb or k: Kilobytes
#       mb or m: Megabytes
#       mbit: Megabits
#       kbit: Kilobits
#  To get the byte figure from bits, divide the number by 8 bit
#

# Run this with sudo previliege

VIRTUAL="ifb0"

# The network interface we're planning on limiting bandwidth.
IF=p4p1             # Interface

# Download limit (in mega bits)
DNLD=4.5mbit

# Client ports
PORTS=($(seq 1024 3024))

SERVER_PORTS=(6443 6444 6445 6446)

# (half the) Delay in ms
DELAY=30

# Upload limit (in mega bits)
UPLD=

#server ip address
REMOTE_IP=10.40.0.100

# We'll use Hierarchical Token Bucket (HTB) to shape bandwidth.

tc_outgoing() {
    if [ -z "$UPLD" ]; then
        return;
    fi
    echo "setting up upload=$UPLD";
    tc qdisc add dev $IF root handle 1: htb
    tc class add dev $IF parent 1: classid 1:1 htb rate $UPLD;
    
    if [ $DELAY -gt 0 ]; then
        echo "- latency ${DELAY}ms";
        tc qdisc add dev $IF parent 1:1 handle 10: netem delay ${DELAY}ms;
    fi
    
    match="match ip dst $REMOTE_IP";
    if ! [ -z "$PORT" ]; then
        match+="match ip sport $PORT 0xffff";
    fi
    
    if [ -z "$match" ]; then
        match="match u32 0 0";
    fi
    
    tc filter add dev $IF protocol ip parent 1: prio 1 u32 $match flowid 1:1;
}

tc_incoming() {
    if [ -z "$DNLD" ]; then
        return;
    fi
    
    echo "setting up download=$DNLD";
    #set up virtual link
    modprobe ifb numifbs=1;
    ip link set dev $VIRTUAL up;
    tc qdisc add dev $IF handle ffff: ingress;
    # Redirecto ingress eth0 to egress ifb0
    tc filter add dev $IF parent ffff: protocol ip u32 match ip src $REMOTE_IP action mirred egress redirect dev $VIRTUAL;
    
    # we will rate ifb0 instead
    tc qdisc add dev $VIRTUAL root handle 2: htb;
    #tc class add dev $VIRTUAL parent 2: classid 2:1 htb rate $DNLD;
    
    if ! [ -z "$PORTS" ]; then
        class_id=0
        for client_port in ${PORTS[@]}
        do
            let "class_id=class_id+1";
            tc class add dev $VIRTUAL parent 2: classid 2:$class_id htb rate $DNLD;
            match="match ip dport $client_port 0xffff";
            # echo "class_id $class_id $match"
            tc filter add dev $VIRTUAL protocol ip parent 2: prio 1 u32 $match flowid 2:$class_id;
        done
    else
        tc class add dev $VIRTUAL parent 2: classid 2:1 htb rate $DNLD;
        if [ $DELAY -gt 0 ]; then
            echo "- latency ${DELAY}ms";
            tc qdisc add dev $VIRTUAL parent 2:1 handle 20: netem delay ${DELAY}ms;
        fi
        match=;
        
        if [ -z "$match" ]; then
            match="match u32 0 0";
        fi
        echo $match;
        
        tc filter add dev $VIRTUAL protocol ip parent 2: prio 1 u32 $match flowid 2:1;
    fi
}

start() {
    echo "emulate network to $REMOTE_IP";
    tc_outgoing;
    tc_incoming;
}

stop() {
    # Stop the bandwidth shaping.
    if [[ $(tc qdisc |grep '^qdisc htb 1:') ]]; then
        tc qdisc del dev $IF root;
    fi
    if [[ $(tc qdisc |grep '^qdisc htb 2:') ]]; then
        tc qdisc del dev $IF handle ffff: ingress;
        tc qdisc del root dev $VIRTUAL
        # Unload the virtual network module
        ip link set dev $VIRTUAL down
        modprobe -r ifb
    fi
}

restart() {
    stop;
    sleep 1;
    start;
}

# Display status of traffic control status.
show() {
    tc qdisc show dev $IF;
}

case "$1" in
    
    start)
        
        echo -n "Starting bandwidth shaping: "
        start
        echo "done"
    ;;
    
    stop)
        
        echo -n "Stopping bandwidth shaping: "
        stop
        echo "done"
    ;;
    
    restart)
        
        echo -n "Restarting bandwidth shaping: "
        restart
        echo "done"
    ;;
    
    show)
        
        echo "Bandwidth shaping status for $IF:"
        show
        echo ""
    ;;
    
    *)
        
        pwd=$(pwd)
        echo "Usage: tc.bash {start|stop|restart|show}"
    ;;
    
esac
exit 0;
