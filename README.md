Repo for building & running experiment regarding RDMA streaming with userver.   
This repo contains 2 submodules: rdma_live_streaming and userver.   
Clone this with: 
```shell
git clone --recurse-submodules
```
`userver/` directory is the reference to `userver` repo.  
`rdma_live_streaming/` directory is the reference to Ben's `kappa` library.  

run `./make.sh` to build these 2 repos and link them to the `bin/server` binary.

`log-generator/` contains the code for generating `httperf's` `wsesslog` files. For ex:

```shell
cd log-generator/
#see this script for how to use
bash gen.sh
```
