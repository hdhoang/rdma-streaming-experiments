#pragma once

#include <iostream>

using namespace std;

class Options {
public:
    int no_sessions = 1;
    int no_streams = 1;
    int no_buffer_requests = 2;
    double zipf_exponent = -0.8;
    int timeout_sec = 1;
    int duration_sec = 2 * 60;
    string out_file = "";

    //
    size_t request_size = 100000;
    double pace_sec = 1.0;
    size_t server_chunk_size = 250000;
    double server_update_interval = 2.0;

    static const Options &instance();
    static void parse(int argc, char **argv);

    Options() = default;
};
