#pragma once

#include <iostream>
#include <cstdlib>
#include <vector>
#include <random>

using namespace std;

class Dist {
public:
    explicit Dist(int sample_size) : _cdf(sample_size, 0) {}

    const vector<double> &cdf_vector()
    {
        return _cdf;
    }

    void print_cdf(ostream &stream) const
    {
        for (auto p : _cdf) {
            stream << p << " ";
        }
        stream << endl;
    }

    /*Return a random rank based on the distribution*/
    int next_rank()
    {
        auto next = random_gen01();
        for (int i = _cdf.size() - 1; i > 0; i--) {
            if (next > _cdf[i] && next <= _cdf[i - 1]) {
                return i;
            }
        }

        return _cdf.size();
    }

protected:
    vector<double> _cdf;
    virtual double random_gen01() = 0;
};
