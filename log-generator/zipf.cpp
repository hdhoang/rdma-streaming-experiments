#include <cstdlib>
#include <vector>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <random>

#include "zipf.hpp"
#include "common.hpp"

using namespace std;

double base_step = 0.1;

Zipf::Zipf(int sample_size) : Dist(sample_size), rd(), gen(rd()), random_gen(0, 1)
{
    auto exponent = Options::instance().zipf_exponent;

    double freq_sum = 0;
    for (int i = 0; i < sample_size; i++) {
        auto freq = pow(i + base_step, exponent);
        _cdf[i] = freq;
        freq_sum += freq;
    }

    //normalize the distribution
    for (int i = 0; i < sample_size; i++) {
        _cdf[i] /= freq_sum;
    }

    //calculate the cdf
    std::sort(_cdf.begin(), _cdf.end(), std::greater<>());
    double cumulative = 0;
    for (int i = sample_size - 1; i >= 0; i--) {
        cumulative += _cdf[i];
        _cdf[i] = cumulative;
    }
}

double Zipf::random_gen01()
{
    return random_gen(gen);
}
