#pragma once

#include <iostream>
#include <cstdlib>
#include <vector>
#include <random>

#include "dist.hpp"

using namespace std;

class Zipf : public Dist {
public:
    Zipf(int sample_size);

protected:
    double random_gen01() override;

private:
    std::random_device rd;
    std::mt19937 gen;
    std::uniform_real_distribution<double> random_gen;
};
