#include <getopt.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <memory>

#include "common.hpp"

using namespace std;

static unique_ptr<Options> def_opts = make_unique<Options>();

const Options &Options::instance()
{
    return *def_opts;
}

void printOptions(const Options &opts)
{
    cerr << "output file = " << opts.out_file << endl;
    cerr << "zipf exponent = " << opts.zipf_exponent << endl;

    cerr << "#streams = " << opts.no_streams << endl;
    cerr << "#sessions = " << opts.no_sessions << endl;
    cerr << "#buffering_requests = " << opts.no_buffer_requests << endl;

    cerr << "session duration = " << opts.duration_sec << " s" << endl;
    cerr << "request size = " << opts.request_size << " B" << endl;
    cerr << "request timeout = " << opts.timeout_sec << " s" << endl;
    cerr << "request pace = " << opts.pace_sec << " s" << endl;
    cerr << "server chunk size = " << opts.server_chunk_size << " B" << endl;
    cerr << "server update interval = " << opts.server_update_interval << " s" << endl;
}

void Options::parse(int argc, char **argv)
{
    static struct option long_options[] = {
        {"streams", required_argument, nullptr, 's'},
        {"request-size", required_argument, nullptr, 'r'},
        {"timeout", required_argument, nullptr, 't'},
        {"sessions", required_argument, nullptr, 'n'},
        {"zipf-exponent", required_argument, nullptr, 'z'},
        {"buffer-requests", required_argument, nullptr, 'b'},
        {"pace", required_argument, nullptr, 'p'},
        {"duration", required_argument, nullptr, 'd'},
        {"output", required_argument, nullptr, 'o'},
        {"server-chunk", required_argument, nullptr, 'c'},
        {"server-interval", required_argument, nullptr, 'i'}
        //
    };

    int opt;
    int option_index = 0;
    while ((opt = getopt_long(argc, argv, "s:n:r:t:e:z:b:p:d:c:i:", long_options, &option_index)) != EOF) {
        switch (opt) {
        case 'c':
            def_opts->server_chunk_size = static_cast<size_t>(std::atoi(optarg));
            break;
        case 'i':
            def_opts->server_update_interval = std::atof(optarg);
            break;
        case 'o':
            def_opts->out_file = std::string(optarg);
            break;
        case 's':
            def_opts->no_streams = std::atoi(optarg);
            break;
        case 'n':
            def_opts->no_sessions = std::atoi(optarg);
            break;
        case 'r':
            def_opts->request_size = static_cast<size_t>(std::atoi(optarg));
            break;
        case 't':
            def_opts->timeout_sec = std::atoi(optarg);
            break;
        case 'z':
            cout << "DASASDASD" << endl;
            def_opts->zipf_exponent = -std::atof(optarg);
            break;
        case 'b':
            def_opts->no_buffer_requests = std::atoi(optarg);
            break;
        case 'p':
            def_opts->pace_sec = std::atof(optarg);
            break;
        case 'd':
            def_opts->duration_sec = std::atoi(optarg);
            break;
        default:
            cerr << "invalid option " << (char)opt << endl;
            exit(1);
        }
    }

    printOptions(Options::instance());
}
