#include <iostream>
#include <chrono>
#include <string>

#include "session.hpp"
#include "common.hpp"

using namespace std;

size_t random_request_sizes[] = {100000, 150000, 200000, 250000};
static int size_i = 0;

static size_t random_size()
{
    size_i = (size_i + 1) % 4;
    return random_request_sizes[size_i];
}

void Request::write(ostream &stream)
{
    stream << path;
    if (pace_sec > 0) {
        stream << " pace_time=" << pace_sec;
    }

    if (Options::instance().timeout_sec > 0) {
        stream << " timeout=" << Options::instance().timeout_sec;
    }

    stream << " headers='Range: bytes=" << to_string(start_byte) << "-" << to_string(end_byte) << '\'' << endl;
}

Session::Session(int stream_id)
{
    const Options &opts = Options::instance();

    this->stream_id = stream_id;
    this->no_requests = static_cast<int>(opts.duration_sec / opts.pace_sec) + opts.no_buffer_requests;
    this->request_size = opts.request_size;

    // this->request_size =random_size();
    double server_bitrate = opts.server_chunk_size / opts.server_update_interval;
    double required_bytes_per_request = server_bitrate * opts.pace_sec;
    this->byte_skip = static_cast<size_t>(static_cast<int>(required_bytes_per_request + 0.5) - this->request_size);
}

void Session::write(ostream &s)
{
    const Options &opts = Options::instance();
    s << "# session for stream " << stream_id << endl;
    size_t curr_byte = 0;
    int buffering = 0;
    for (int32_t i = 0; i < no_requests; i++) {
        size_t next_byte = curr_byte + this->request_size;
        double pace = buffering++ < opts.no_buffer_requests ? -1 : opts.pace_sec;
        Request r{"/s/" + to_string(stream_id), curr_byte, next_byte - 1, pace};
        if (this->byte_skip > 0) {
            next_byte += this->byte_skip;
        }
        curr_byte = next_byte;
        r.write(s);
    }
    //empty line to mark end of session
    s << endl;
}
