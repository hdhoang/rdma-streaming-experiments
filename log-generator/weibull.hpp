#pragma once

#include <random>
#include <set>
#include <cmath>
#include <iomanip>
#include <algorithm>
#include <cassert>
#include <functional>
#include <random>

#include "dist.hpp"

using namespace std;

class Weibull : public Dist {
public:
    Weibull(int sample_size) : Dist(sample_size), rd(), gen(rd()), random_gen()
    {
        double sum = 0;
        //normalize the distribution
        for (int i = 0; i < sample_size; i++) {
            _cdf[i] = cal_prob(i + 1);
            sum += _cdf[i];
        }

        for (int i = 0; i < sample_size; i++) {
            _cdf[i] /= sum;
        }

        double cumulative = 0;
        for (int i = sample_size - 1; i >= 0; i--) {
            cumulative += _cdf[i];
            _cdf[i] = cumulative;
        }
    }

protected:
    double random_gen01() override
    {
        return random_gen(gen);
    }

private:
    std::random_device rd;
    std::mt19937 gen;
    std::uniform_real_distribution<double> random_gen;

    double cal_prob(double rank)
    {
        const double shape = 0.513;
        const double scale = 4;
        double first = shape / scale;
        double second = std::pow(rank / scale, shape - 1);
        double third_pow = std::pow(rank / scale, shape);
        double third = std::exp(-third_pow);

        return first * second * third;
    }
};
