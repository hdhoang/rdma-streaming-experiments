#pragma once

#include <iostream>
#include <chrono>
#include <string>

using namespace std;

class Request {
public:
    const string path;
    size_t start_byte;
    size_t end_byte;
    double pace_sec;

    void write(ostream &stream);
};

class Session {
public:
    int stream_id;

    Session(int stream_id);

    void write(ostream &s);

private:
    int no_requests;
    size_t request_size;
    size_t byte_skip;
};
