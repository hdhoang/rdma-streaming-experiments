#!/bin/bash

#ports=(6443 6444 6445 6446)
ports=(6443)

make -j;

function gen_port(){
    ./bin/main --output=requests_$1.log \
    --streams=1 \
    --sessions=2000 \
    --request-size=250000 \
    --timeout=1 \
    --zipf-exponent=0.95 \
    --buffer-requests=1 \
    --pace=1 \
    --duration=80 \
    --server-chunk=500000 \
    --server-interval=1.0 \
    ;
}

for i in "${ports[@]}"
do
    gen_port $i;
done
