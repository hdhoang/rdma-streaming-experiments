#include <iostream>
#include <chrono>
#include <string>
#include <getopt.h>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <memory>

#include "session.hpp"
#include "common.hpp"
#include "zipf.hpp"
#include "weibull.hpp"

using namespace std;

void print_stats(vector<int> &session_count)
{
    static int tops[] = {1, 5, 10, 20, 50, 200, 500, 1000, 5000, -1};
    int no_sessions = Options::instance().no_sessions;
    int no_streams = Options::instance().no_streams;

    cerr << "Stream popularity" << endl;
    cerr << "# top_streams   #sessions   %sessions" << endl;
    for (int t = 0; tops[t] > 0; t++) {
        if (tops[t] >= no_streams) {
            tops[t] = no_streams;
            tops[t + 1] = -1;
        }

        int total = 0;
        int i = 0;
        for (i = 0; i < tops[t] && i < no_sessions; i++) {
            total += session_count[i];
        }
        fprintf(stderr, "%13d   %9d   %9.1f\n", i, total, 100.0 * total / no_sessions);
    }
}

static shared_ptr<Dist> get_dist(int sample_size)
{
    return make_shared<Zipf>(sample_size);
}

int main(int argc, char **argv)
{
    Options::parse(argc, argv);
    const Options &opts = Options::instance();
    ofstream out_file;
    bool write_stdout = true;
    if (opts.out_file.length() > 0) {
        out_file.open(opts.out_file, ios::out);
        if (out_file.fail()) {
            cerr << "failed to open file " << opts.out_file << endl;
            exit(1);
        }
        write_stdout = false;
    }

    auto dist = get_dist(opts.no_streams);
    // dist->print_cdf(cout);

    //mapping of rank->session count
    vector<int> session_count(opts.no_streams, 0);

    for (int i = 0; i < opts.no_sessions; i++) {
        auto sid = dist->next_rank() - 1;
        Session s(sid);
        session_count[s.stream_id]++;
        // printf("sid %d\n", sid);
        s.write(write_stdout ? cout : out_file);
    }

    cerr << endl;
    print_stats(session_count);
    if (!write_stdout) {
        out_file.close();
    }
    return 0;
}
