#include <sys/socket.h>
#include <sys/mman.h>
#include <sys/un.h>
#include <cstring>
#include <unistd.h>

#include "protocol.hpp"
#include "client.h"

class ServerConn {
private:
    pid_t shared_pid = -1;
    int conn_fd = -1;
    int mem_fd = -1;

    /* virtual address of vid buffer on the shared node */
    uint8_t *shared_buf;

    /* virtual address of vid buffer on the client */
    uint8_t *vid_buf;

    ProtoWriter send_writer;

    uint8_t recv_buf[PROTO_MAX_SIZE];

    struct msghdr recv_msg;

    char c_buffer[256];

    struct iovec io;

    void recv_msg_reset()
    {
        memset(&recv_msg, 0, sizeof(recv_msg));
        io.iov_base = recv_buf;
        io.iov_len = PROTO_MAX_SIZE;
        recv_msg.msg_iov = &io;
        recv_msg.msg_iovlen = 1;
        recv_msg.msg_control = c_buffer;
        recv_msg.msg_controllen = sizeof(c_buffer);
    }

    int msg_get_fd()
    {
        if (recv_msg.msg_controllen <= 0) {
            return -1;
        }

        struct cmsghdr *cmsg = CMSG_FIRSTHDR(&recv_msg);
        if (cmsg->cmsg_len <= 0) {
            return -1;
        }
        unsigned char *data = CMSG_DATA(cmsg);
        int fd = *((int *)data);
        return fd;
    }

    int server_connect(const char *server_name)
    {
        int sd = socket(AF_UNIX, SOCK_STREAM, 0);
        struct sockaddr_un server;
        server.sun_family = AF_UNIX;
        strcpy(server.sun_path, server_name);
        int rc = -1, trial = 0;
        while (trial < 10) {
            rc = connect(sd, (struct sockaddr *)&server, sizeof(struct sockaddr_un));
            if (rc >= 0) {
                break;
            }

            //sleep and re-try
            trial++;
            sleep(trial);
        }

        if (rc < 0) {
            close(sd);
            fprintf(stderr, "connecting Unix socket ");
            perror(server_name);
            return rc;
        }

        return sd;
    }

    int send_msg(int send_fd, void *data, size_t data_len)
    {
        struct msghdr msg = {0};
        char buf[CMSG_SPACE(sizeof(send_fd))];
        memset(buf, '\0', sizeof(buf));
        struct iovec io = {
            .iov_base = data,
            .iov_len = data_len //
        };

        msg.msg_iov = &io;
        msg.msg_iovlen = 1;

        if (send_fd > 0) {
            msg.msg_control = buf;
            msg.msg_controllen = sizeof(buf);
            struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg);
            cmsg->cmsg_level = SOL_SOCKET;
            cmsg->cmsg_type = SCM_RIGHTS;
            cmsg->cmsg_len = CMSG_LEN(sizeof(send_fd));
            *((int *)CMSG_DATA(cmsg)) = send_fd;
            msg.msg_controllen = cmsg->cmsg_len;
        }

        int rc = sendmsg(conn_fd, &msg, 0);
        if (rc < 0) {
            return rc;
        }

        return 0;
    }

public:
    pid_t create_server(int node_id, const char *bin, char *const config_file)
    {
        shared_pid = fork();
        if (shared_pid != 0) {
            //parent
            return shared_pid;
        }

        // child
        // int fd = open(file, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);

        // dup2(fd, 1); // make stdout go to file
        // dup2(fd, 2); // make stderr go to file - you may choose to not do this
        //              // or perhaps send stderr to another file

        // close(fd); // fd no longer needed - the dup'ed handles are sufficient

        char buffer[64];
        sprintf(buffer, "%d", node_id);
        printf("launching %s delivery with args: %s %s %s\n", bin, SERVER_NAME, buffer, config_file);
        char *const argv[] = {
            SERVER_NAME,
            SERVER_NAME,
            buffer,
            config_file,
            NULL
            //
        };
        execvp(bin, argv);
        perror("execvp");
        return -1;
    }

    int init_server()
    {
        conn_fd = server_connect(SERVER_NAME);
        if (conn_fd < 0) {
            return conn_fd;
        }

        //get the server buffer address and mem_fd
        recv_msg_reset();
        send_writer.reset();
        send_writer.write(get_buffer_info);

        int rc = send_msg(-1, send_writer.buf(), send_writer.size());
        if (rc < 0) {
            perror("stream_init::send_msg");
            return rc;
        }

        ssize_t r_size = recvmsg(conn_fd, &recv_msg, 0);
        if (r_size < 0) {
            perror("stream_init::recvmsg");
            return -1;
        }
        mem_fd = msg_get_fd();

        size_t buf_size = 0;
        ProtoReader reader(recv_buf);
        shared_buf = reader.read<uint8_t *>();
        buf_size = reader.read<size_t>();

        printf("mem_fd %d, buf %p, size %zd\n", mem_fd, shared_buf, buf_size);

        vid_buf = (uint8_t *)mmap(NULL, buf_size, PROT_READ, MAP_SHARED, mem_fd, 0);
        if (vid_buf == MAP_FAILED) {
            perror("mmap");
            return -1;
        }

        printf("mmaped to %p\n", vid_buf);
        return 0;
    }
};
