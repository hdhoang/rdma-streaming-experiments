#include <sys/uio.h>
#include <inttypes.h>

typedef void *shared_server;

//fork and exec the shared delivery,
shared_server stream_create_server(int node_id, const char *bin, char *const config_file);

//close the shared delivery
void stream_close_server(shared_server server);

//to be called after forking and before entering the main server loop
//this will initialize a connection to the shared delivery process
//returns 0 on success and -1 on failure
int stream_init(shared_server server);

//check if a stream exist
int stream_exist(shared_server server, int stream_id);

//get current offset
off_t stream_current_offset(shared_server server, int stream_id);

//get vid buffers, ret 0 on success, -1 on fail
int stream_getbuffers(shared_server server, int stream_id, struct iovec *vecs, int vec_len, off_t start, size_t length);

//get details about a chunk
int stream_chunk_details(shared_server server, int stream_id, struct iovec vec, uint8_t *start_cons, uint8_t *end_cons, int64_t *serialno);
