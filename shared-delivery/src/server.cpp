#include <cstring>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/mman.h>
#include <thread>
#include <mutex>
#include <cstdarg>

#include "server.hpp"

//get the fd sent over unix socket
static inline int msg_get_fd(struct msghdr &msg)
{
    if (msg.msg_controllen <= 0) {
        return -1;
    }

    struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg);
    if (cmsg->cmsg_len <= 0) {
        return -1;
    }
    unsigned char *data = CMSG_DATA(cmsg);
    int fd = *((int *)data);
    return fd;
}

std::mutex log_mutex;

void Server::log(const char *fmt, ...)
{
    std::lock_guard<std::mutex> lock(log_mutex);
    va_list args;
    va_start(args, fmt);
    printf("[Sdelivery] ");
    vprintf(fmt, args);
    va_end(args);
}

Server::Server(const char *server_name, Streaming *stream)
    : _name(server_name), _sd(-1), _stream(stream)
{
    struct sockaddr_un server = {0};
    //open unix domain socket
    auto server_size = std::strlen(server_name);
    if (server_size < 1 || server_size > sizeof(server.sun_path)) {
        fprintf(stderr, "server name must have size less than %zd\n", sizeof(server.sun_path));
        throw std::exception();
    }

    //just try to unlink the name
    unlink(server_name);

    _sd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (_sd < 0) {
        perror("socket");
        throw std::exception();
    }
    server.sun_family = AF_UNIX;
    strcpy(server.sun_path, server_name);
    printf("server: %s\n", server.sun_path);
    if (bind(_sd, (struct sockaddr *)&server, sizeof(struct sockaddr_un))) {
        perror("binding stream socket");
        throw std::exception();
    }

    log("Socket %d has name %s\n", _sd, server.sun_path);
}

Server::~Server()
{
    close(_sd);
    unlink(_name.data());
}

void Server::send_msg(int conn_fd, int send_fd, void *data, size_t data_len)
{
    struct msghdr msg = {0};
    char buf[CMSG_SPACE(sizeof(send_fd))];
    memset(buf, '\0', sizeof(buf));
    struct iovec io = {
        .iov_base = data,
        .iov_len = data_len //
    };

    msg.msg_iov = &io;
    msg.msg_iovlen = 1;

    if (send_fd > 0) {
        msg.msg_control = buf;
        msg.msg_controllen = sizeof(buf);
        struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg);
        cmsg->cmsg_level = SOL_SOCKET;
        cmsg->cmsg_type = SCM_RIGHTS;
        cmsg->cmsg_len = CMSG_LEN(sizeof(send_fd));
        *((int *)CMSG_DATA(cmsg)) = send_fd;
        msg.msg_controllen = cmsg->cmsg_len;
    }

    if (sendmsg(conn_fd, &msg, 0) < 0) {
        perror("sendmsg");
    }
}

void Server::handle_getbufinfo(ProtoReader &reader, ProtoWriter &writer)
{
    writer.write(_stream->buf());
    writer.write(_stream->bufsize());
}

void Server::handle_stream_exist(ProtoReader &reader, ProtoWriter &writer)
{
    int stream_id = reader.read<int>();
    uint8_t ret = _stream->stream_exist(stream_id) ? 1 : 0;
    writer.write(ret);
}

void Server::handle_current_offset(ProtoReader &reader, ProtoWriter &writer)
{
    int stream_id = reader.read<int>();
    off_t ret = _stream->stream_offset(stream_id);
    writer.write(ret);
}

void Server::handle_chunk_details(ProtoReader &reader, ProtoWriter &writer)
{
    auto stream_id = reader.read<int>();
    auto piece = reader.read<iovec>();

    uint8_t start_cons;
    uint8_t end_cons;

    int rc = _stream->chunk_details(stream_id, piece, &start_cons, &end_cons);
    writer.write(rc);
    if (rc < 0) {
        return;
    }

    writer.write(start_cons);
    writer.write(end_cons);
}

void Server::handle_stream_vid_buffers(ProtoReader &reader, ProtoWriter &writer)
{
    auto stream_id = reader.read<int>();
    auto offset = reader.read<off_t>();
    auto length = reader.read<size_t>();

    struct iovec vecs[5]; //probably enough
    int rc = _stream->get_vid_buffers(stream_id, vecs, 5, offset, length);

    writer.write(rc);
    if (rc < 0) {
        //error, stopped writing
        return;
    }

    for (int i = 0; i < rc; i++) {
        struct iovec vec = vecs[i];
        writer.write(vec);
    }
}

void Server::handle_connection(int conn_fd)
{
    //struct for receiving messages
    struct msghdr msg = {0};
    char m_buffer[PROTO_MAX_SIZE];
    char c_buffer[256];
    struct iovec io = {0};
    ProtoReader reader(m_buffer);
    ProtoWriter writer;

    auto reset_recv = [&msg, &m_buffer, &c_buffer, &io]() {
        io.iov_base = m_buffer;
        io.iov_len = PROTO_MAX_SIZE;
        msg.msg_iov = &io;
        msg.msg_iovlen = 1;
        msg.msg_control = c_buffer;
        msg.msg_controllen = sizeof(c_buffer);
    };

    while (true) {
        try {
            int ret_fd = -1;
            reset_recv();
            auto r_size = recvmsg(conn_fd, &msg, 0);
            if (r_size < 0) {
                perror("recvmsg");
                throw std::runtime_error(std::strerror(errno));
            } else if (r_size == 0) {
                //client closed connection
                break;
            }
            reader.reset();
            writer.reset();

            auto opcode = reader.read<Ops>();
            // printf("op %d\n", opcode);
            switch (opcode) {
            default:
                throw std::runtime_error("unrecognized opcode " + std::to_string((int)opcode));
                break;
            case Ops::get_buffer_info:
                handle_getbufinfo(reader, writer);
                ret_fd = _stream->memfd();
                break;
            case Ops::get_stream_exist:
                handle_stream_exist(reader, writer);
                break;
            case Ops::get_current_offset:
                handle_current_offset(reader, writer);
                break;
            case Ops::get_chunk_details:
                handle_chunk_details(reader, writer);
                break;
            case Ops::get_stream_vid_buffers:
                handle_stream_vid_buffers(reader, writer);
                break;
            }
            // printf("writer buf %d\n", *(int *)writer.buf());
            send_msg(conn_fd, ret_fd, writer.buf(), writer.size());
        } catch (std::runtime_error &ex) {
            log("Conn %d thread caught exception: %s\n", conn_fd, ex.what());
            continue;
        }
    }

    log("closing connection %d\n", conn_fd);
    close(conn_fd);
}

static void connection_thread(Server *sv, int sock)
{
    sv->handle_connection(sock);
}

void Server::start()
{
    //start listening
    listen(_sd, 5);
    while (true) {
        int msgsock = accept(_sd, 0, 0);
        log("accepted %d\n", msgsock);
        if (msgsock < 0) {
            perror("accept");
            continue;
        }

        //start a thread to handle the connection
        std::thread t(connection_thread, this, msgsock);
        t.detach();
    }
}
