#include <sys/uio.h>
#include <unistd.h>

#include "stream.hpp"
#include "kappa/kappa_c.hpp"

void *fake_mem = nullptr;
int mem_fd;

struct stream_data {
    KappaCOfstream *of_stream;
    KappaCOutputSync *out_sync;
    KappaC *kappa_c;

    stream_data(int node_id)
    {
        of_stream = KappaCOfstreamNew("/dev/stdout");
        out_sync = KappaCOutputSyncNew(of_stream);
        kappa_c = KappaCNew(out_sync, node_id);
    }

    ~stream_data()
    {
        KappaCDelete(kappa_c);
        KappaCOutputSyncDelete(out_sync);
        KappaCOfstreamDelete(of_stream);
    }

    static inline stream_data *cast(void *data)
    {
        return static_cast<stream_data *>(data);
    }
};

Streaming::Streaming(int node_id, const char *config) : _node_id(node_id), _config_file(config)
{
    _data = new stream_data(node_id);
}

Streaming::~Streaming()
{
    delete stream_data::cast(_data);
}

void Streaming::init()
{
    // mem_fd = syscall(319, "foo", 0);
    // ftruncate(mem_fd, 250000 * 8 * 10);
    // fake_mem = mmap(nullptr, 250000 * 8 * 10, PROT_READ | PROT_WRITE, MAP_SHARED, mem_fd, 0);
    // return;

    int rc = KappaCInitialize(stream_data::cast(_data)->kappa_c, _config_file);
    if (rc < 0) {
        throw std::runtime_error("Stream init failed");
    }

    // printf("\n****LIVESTREAM_CONFIG****\n");
    // KappaCPrintConfig(stream_data::cast(_data)->kappa_c);
    KappaCBarrier(stream_data::cast(_data)->kappa_c);
}

void Streaming::close()
{
    auto kappa_c = stream_data::cast(_data)->kappa_c;
    KappaCBarrier(kappa_c);
    KappaCBarrier(kappa_c);
    KappaCPrintNetworkStats(kappa_c);
}

void *Streaming::buf() const
{
    // return fake_mem;

    iovec vec;
    int rc = KappaCVideoBuffer(stream_data::cast(_data)->kappa_c, _node_id, &vec, nullptr, nullptr);
    if (rc < 0) {
        return nullptr;
    }
    return vec.iov_base;
}

int Streaming::memfd() const
{
    // return mem_fd;
    return KappaCSharedVideoBufferFile(stream_data::cast(_data)->kappa_c);
}

size_t Streaming::bufsize() const
{
    // return 250000 * 8 * 10;

    iovec vec;
    int rc = KappaCVideoBuffer(stream_data::cast(_data)->kappa_c, _node_id, &vec, nullptr, nullptr);
    if (rc < 0) {
        return 0;
    }
    return vec.iov_len;
}

bool Streaming::stream_exist(int stream_id) const
{
    // return true;
    auto kappa_c = stream_data::cast(_data)->kappa_c;
    auto stream = KappaCGetStreamInfo(kappa_c, stream_id);
    return stream != nullptr;
}

off_t Streaming::stream_offset(int stream_id) const
{
    // return 0;
    auto kappa_c = stream_data::cast(_data)->kappa_c;
    auto stream = KappaCGetStreamInfo(kappa_c, stream_id);
    auto offset = KappaCStreamInfoCurrentOffset(kappa_c, stream);
    return offset;
}

int Streaming::get_vid_buffers(int stream_id, struct iovec *vecs, int vecs_len, off_t offset, size_t length) const
{
    // vecs[0].iov_base = fake_mem;
    // vecs[0].iov_len = length;
    // return 1;
    auto kappa_c = stream_data::cast(_data)->kappa_c;
    auto stream = KappaCGetStreamInfo(kappa_c, stream_id);
    return KappaCStreamInfoVideoBuffers(kappa_c, stream, vecs, vecs_len, offset, length);
}

int Streaming::chunk_details(int stream_id, iovec chunk_buf, uint8_t *start_cons, uint8_t *end_cons) const
{
    // *start_cons = 1;
    // *end_cons = 1;
    // *serialno = 1;
    // return 0;

    auto kappa_c = stream_data::cast(_data)->kappa_c;
    auto stream = KappaCGetStreamInfo(kappa_c, stream_id);
    return KappaCStreamInfoChunkDetails(kappa_c, stream, chunk_buf, start_cons, end_cons);
}
