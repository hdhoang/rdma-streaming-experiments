#include <iostream>
#include <thread>
#include <chrono>
#include <memory>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <signal.h>

#include "protocol.hpp"
#include "server.hpp"

using namespace std::chrono_literals;

static Streaming *stream;
static bool exiting = false;
// static std::unique_ptr<Server> server;

static void sig_kill_handler(int something)
{
    if (exiting == true) {
        return;
    }
    exiting = true;
    Server::log("Sdelivery terminating\n");
    stream->close();
    exit(0);
}

static void reg_sigkill(int sig)
{
    struct sigaction act;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    act.sa_handler = sig_kill_handler;
    if (sigaction(sig, &act, nullptr) < 0) {
        perror("sigaction");
    }
}

int main(int argc, char const *argv[])
{
    if (argc < 4) {
        Server::log("[server_name] [node_id] [config]\n");
        exit(1);
    }

    auto server_name = argv[1];
    int node_id = std::atoi(argv[2]);
    auto config_file = argv[3];

    // reg_sigkill(SIGTERM);
    reg_sigkill(SIGINT);
    try {
        stream = new Streaming(node_id, config_file);
        stream->init();
        Server::log("stream init done\n");

        Server server(server_name, stream);
        server.start();
    } catch (std::exception &e) {
        Server::log("exp %s\n", e.what());
        return EXIT_FAILURE;
    }

    return 0;
}
