#pragma once
#include <iostream>
#include <cstddef>
#include <cstdint>
#include <cstring>

#define PROTO_MAX_SIZE 64 * 10
#define SERVER_NAME "sdelivery"

enum Ops : uint8_t {
    //get the info about vid buffer and memfd
    //() -> (int mem_fd, uint8_t* buf_ptr, size_t size, size_t chunk_size)
    get_buffer_info,

    //get current offset of a stream:
    //(int stream_id) -> off_t
    get_current_offset,

    //get the iovecs that points to the stream segment
    //(int stream_id, off_t stream_offset, size_t length) -> (int rc, iovec[] vecs)
    get_stream_vid_buffers,

    //get the details of a chunk, such as consistency values
    //(int stream_id, int64_t serial) -> (int rc, char start_consistency, char end_consistency)
    get_chunk_details,

    //does the stream exist
    //(int stream_id) -> bool
    get_stream_exist
};

class ProtoWriter {
public:
    ProtoWriter() : _size(0), _buf()
    {
        std::memset(_buf, 0, PROTO_MAX_SIZE);
    }

    template <typename t>
    void write(t val)
    {
        auto new_size = _size + sizeof(t);
        if (new_size > PROTO_MAX_SIZE) {
            std::cerr << "Size exceed " << PROTO_MAX_SIZE << std::endl;
            throw std::exception();
        }

        t *ptr = (t *)((uint8_t *)_buf + _size);
        *ptr = val;
        _size = new_size;
    }

    size_t size() const
    {
        return _size;
    }

    void *buf()
    {
        return static_cast<void *>(_buf);
    }

    void reset()
    {
        _size = 0;
    }

private:
    size_t _size;
    uint8_t _buf[PROTO_MAX_SIZE];
};

class ProtoReader {
public:
    ProtoReader(void *buf) : _buf(static_cast<uint8_t *>(buf)), _off(0) {}

    void reset()
    {
        _off = 0;
    }

    template <typename t>
    t read()
    {
        auto new_off = _off + sizeof(t);
        if (new_off > PROTO_MAX_SIZE) {
            std::cerr << "Size exceed " << PROTO_MAX_SIZE << std::endl;
            throw std::exception();
        }

        t *ptr = reinterpret_cast<t *>(_buf + _off);
        _off = new_off;
        return *ptr;
    }

private:
    uint8_t *_buf;
    size_t _off;
};
