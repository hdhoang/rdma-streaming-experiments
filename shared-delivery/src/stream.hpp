#pragma once

#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <cerrno>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <unistd.h>

#ifndef SYS_memfd_create
#define SYS_memfd_create 319
#endif

class Streaming {
public:
    Streaming(int node_id, const char *config);

    ~Streaming();

    void init();

    void close();

    void *buf() const;

    int memfd() const;

    size_t bufsize() const;

    bool stream_exist(int stream_id) const;

    off_t stream_offset(int stream_id) const;

    int get_vid_buffers(int stream_id, struct iovec *vecs, int vecs_len, off_t offset, size_t length) const;

    int chunk_details(int stream_id, iovec chunk_buf, uint8_t *start_cons, uint8_t *end_cons) const;

private:
    int _node_id = -1;
    const char *_config_file;
    void *_data = nullptr;
};
