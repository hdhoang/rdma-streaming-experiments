#pragma once

#include <cstdint>
#include <string>
#include <memory>

#include "protocol.hpp"
#include "stream.hpp"

class Server {
public:
    //create the server
    Server(const char *server_name, Streaming *stream);
    ~Server();

    //start listening
    void start();

    //handle connection
    void handle_connection(int conn_fd);

    static void log(const char *fmt, ...);

private:
    std::string _name;
    int _sd;
    Streaming *_stream;

    //send a file descriptor
    void send_msg(int conn_fd, int fd, void *buf, size_t data_len);

    void handle_getbufinfo(ProtoReader &reader, ProtoWriter &writer);

    void handle_stream_exist(ProtoReader &reader, ProtoWriter &writer);

    void handle_current_offset(ProtoReader &reader, ProtoWriter &writer);

    void handle_chunk_details(ProtoReader &reader, ProtoWriter &writer);

    void handle_stream_vid_buffers(ProtoReader &reader, ProtoWriter &writer);
};
