bin="dotnet vmstat-processor/bin/Release/netcoreapp2.2/publish/vmstat-processor.dll"
column=0;
output=redis_1sub;
path="/home/hdhoang/rs/tput/$output";
sizes=(8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288);
targets="";
for s in "${sizes[@]}"
do 
    targets=$targets\ $s,$path/$s.txt;
done
$bin --column $column --target $targets > $output.txt
