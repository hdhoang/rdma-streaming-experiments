iperf -c 10.70.0.1 -B 10.70.0.100 -i1 -P1 -t50 &
iperf -c 10.70.1.2 -B 10.70.1.100 -i1 -P2 -t50  &
iperf -c 10.70.2.3 -B 10.70.2.100 -i1 -P2 -t50  &
iperf -c 10.70.3.5 -B 10.70.3.100 -i1 -P2 -t50  &

trap 'kill $( jobs -p ); exit' SIGINT

wait
