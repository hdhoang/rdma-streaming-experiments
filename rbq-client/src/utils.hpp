#pragma once

#include <string>
#include <cstdint>
#include <amqp.h>

#define MAXRATES 200
#define MSG_SIZE 32
#define BATCH_MEASURE 3000
#define RECORD_SIZE 1024 * 1024 * 10
#define SERVER_PORT 5672
#define THREADS 10

extern std::string ping_proc;
extern std::string pong_proc;
extern std::string pub_proc;
extern std::string sub_proc;

void die(const char *fmt, ...);
extern void die_on_error(int x, char const *context);
extern void die_on_amqp_error(amqp_rpc_reply_t x, char const *context);

extern void amqp_dump(void const *buffer, size_t len);

extern uint64_t now_microseconds(void);
extern void microsleep(int usec);
