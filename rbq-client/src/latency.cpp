#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <thread>
#include <iostream>
#include <fstream>
#include <atomic>
#include <chrono>
#include <vector>
#include <cstring>

#include <amqp.h>
#include <amqp_tcp_socket.h>
#include "utils.hpp"

// messages / miliseconds
#define SEND_RATE 10

using namespace std::chrono_literals;

static std::atomic<int64_t> rates[MAXRATES];

static std::vector<int> l_records[10];

static char Payload[MSG_SIZE];
static char const *default_exchange = "";

const char *broker_address;

std::atomic<bool> send_stop = false;
std::atomic<bool> l_recording = false;

static inline int64_t usec_timestamp()
{
    return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
}

static void ping_consume_thread(amqp_connection_state_t conn, int ping_id)
{
    amqp_envelope_t envelope;

    while (!send_stop.load())
    {
        struct timeval tv;
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        amqp_maybe_release_buffers(conn);
        amqp_rpc_reply_t res = amqp_consume_message(conn, &envelope, &tv, 0);
        if (AMQP_RESPONSE_NORMAL != res.reply_type)
        {
            continue;
        }

        // if (envelope.message.body.len != MSG_SIZE)
        // {
        //     die("wrong msg size %zd\n", envelope.message.body.len);
        // }

        rates[ping_id]++;
        if (l_recording.load())
        {
            int64_t sent_timestamp = *(int64_t *)(envelope.message.body.bytes);
            int latency_usec = (int)(usec_timestamp() - sent_timestamp);
            // printf("latency %d\n", latency_usec);
            l_records[ping_id].push_back(latency_usec);
        }
    }
}

static void ping(amqp_connection_state_t conn, int ping_id)
{
    std::string ping_key = std::to_string(ping_id);
    std::string pong_key = std::to_string(ping_id + 10);

    amqp_bytes_t message_bytes;
    message_bytes.bytes = Payload;
    message_bytes.len = MSG_SIZE;
    amqp_basic_properties_t props;
    props._flags = AMQP_BASIC_DELIVERY_MODE_FLAG;
    props.delivery_mode = 1;

    //declare "pong" queue
    amqp_queue_declare_ok_t *r = amqp_queue_declare(
        conn, 1, amqp_cstring_bytes(pong_key.c_str()), 0, 0, 1, 1, amqp_empty_table);

    die_on_amqp_error(amqp_get_rpc_reply(conn), "Declaring queue");
    amqp_bytes_t pong_q_name = amqp_bytes_malloc_dup(r->queue);

    //consume pong
    amqp_basic_consume(conn, 1, pong_q_name, amqp_empty_bytes,
                       0, 1, 0, amqp_empty_table);

    printf("consuming %s size %d\n", pong_key.c_str(), MSG_SIZE);
    die_on_amqp_error(amqp_get_rpc_reply(conn), "Consuming");
    std::thread consume_thread(ping_consume_thread, conn, ping_id);

    //start sending
    int64_t sent = 0;
    std::chrono::high_resolution_clock::time_point start_point = std::chrono::high_resolution_clock::now();
    while (!send_stop.load())
    {
        auto period = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start_point);
        int64_t expected_sent = period.count() * SEND_RATE;
        if (sent > expected_sent)
        {
            std::this_thread::sleep_for(1us);
            continue;
        }

        int64_t timestamp = usec_timestamp();
        memcpy(Payload, &timestamp, sizeof(timestamp));

        // start by pinging
        die_on_error(amqp_basic_publish(conn, 1, amqp_cstring_bytes(default_exchange),
                                        amqp_cstring_bytes(ping_key.c_str()), 0, 0,
                                        &props, message_bytes),
                     "Publishing");

        sent++;
    }

    printf("send thread %d prepare to exit\n", ping_id);
    consume_thread.join();
}

static void pong(amqp_connection_state_t conn, int ping_id)
{
    std::string ping_key = std::to_string(ping_id);
    std::string pong_key = std::to_string(ping_id + 10);

    amqp_bytes_t message_bytes;
    message_bytes.bytes = Payload;
    message_bytes.len = MSG_SIZE;

    amqp_basic_properties_t props;
    props._flags = AMQP_BASIC_DELIVERY_MODE_FLAG;
    props.delivery_mode = 1;

    //declare "ping" queue
    amqp_queue_declare_ok_t *r = amqp_queue_declare(
        conn, 1, amqp_cstring_bytes(ping_key.c_str()), 0, 0, 1, 1, amqp_empty_table);

    die_on_amqp_error(amqp_get_rpc_reply(conn), "Declaring queue");
    amqp_bytes_t ping_q_name = amqp_bytes_malloc_dup(r->queue);

    printf("consuming %s size %d\n", ping_key.c_str(), MSG_SIZE);

    //consume ping
    amqp_basic_consume(conn, 1, ping_q_name, amqp_empty_bytes,
                       0, 1, 0, amqp_empty_table);

    die_on_amqp_error(amqp_get_rpc_reply(conn), "Consuming");

    amqp_envelope_t envelope;
    amqp_rpc_reply_t res;
    while (true)
    {
        // start by receiving
        amqp_maybe_release_buffers(conn);
        do
        {
            res = amqp_consume_message(conn, &envelope, NULL, 0);
            if (AMQP_RESPONSE_NORMAL != res.reply_type)
            {
                continue;
            }
            if (envelope.message.body.len != MSG_SIZE)
            {
                die("wrong msg size %zd\n", envelope.message.body.len);
            }

            memcpy(Payload, envelope.message.body.bytes, MSG_SIZE);
        } while (AMQP_RESPONSE_NORMAL != res.reply_type);
        rates[ping_id]++;

        //reply
        die_on_error(amqp_basic_publish(conn, 1, amqp_cstring_bytes(default_exchange),
                                        amqp_cstring_bytes(pong_key.c_str()), 0, 0,
                                        &props, message_bytes),
                     "Publishing");
    }
}

static void pinger(bool req, int ping_id)
{
    int port, status;
    amqp_socket_t *socket = NULL;
    amqp_connection_state_t conn;

    char const *hostname = broker_address;
    port = 5672;

    conn = amqp_new_connection();

    socket = amqp_tcp_socket_new(conn);
    if (!socket)
    {
        die("creating TCP socket");
    }

    status = amqp_socket_open(socket, hostname, port);
    if (status)
    {
        die("opening TCP socket");
    }

    die_on_amqp_error(amqp_login(conn, "/", 0, 131072, 0, AMQP_SASL_METHOD_PLAIN,
                                 "guest", "guest"),
                      "Logging in");
    amqp_channel_open(conn, 1);
    die_on_amqp_error(amqp_get_rpc_reply(conn), "Opening channel");

    if (req)
    {
        ping(conn, ping_id);
    }
    else
    {
        pong(conn, ping_id);
    }

    die_on_amqp_error(amqp_channel_close(conn, 1, AMQP_REPLY_SUCCESS),
                      "Closing channel");
    die_on_amqp_error(amqp_connection_close(conn, AMQP_REPLY_SUCCESS),
                      "Closing connection");
    die_on_error(amqp_destroy_connection(conn), "Ending connection");
}

// static void replier(){}

static void sampling_thread_func(int topic_count)
{
    uint64_t count = 0;
    while (true)
    {
        std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
        std::this_thread::sleep_for(2s);

        uint64_t new_count = 0;
        for (int i = 0; i < topic_count; i++)
        {
            new_count += rates[i].load();
        }
        auto dur = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - start);
        printf("rate %zd msg/s\n", (new_count - count) / dur.count());
        count = new_count;
    }
}

static std::vector<std::thread> threads;
static std::thread sampling_thread;

int latency(int argc, char const *const *argv)
{
    sampling_thread = std::thread(sampling_thread_func, 20);

    broker_address = argv[2];

    for (int i = 0; i < THREADS; i++)
    {
        l_records[i].reserve(RECORD_SIZE);
        if (ping_proc.compare(argv[1]) == 0)
        {
            threads.push_back(std::thread(&pinger, true, i));
        }
        else
        {
            threads.push_back(std::thread(&pinger, false, i));
        }
    }

    printf("press [Enter] to begin RECORDING\n");
    while (std::cin.get() != '\n')
        ;
    l_recording = true;

    // run for 120s
    std::this_thread::sleep_for(10s);

    send_stop = true;

    for (auto &t : threads)
    {
        t.join();
    }

    printf("sender threads stopped\n");
    printf("before finish wait for records to finalize\n");
    std::this_thread::sleep_for(3s);

    for (int i = 0; i < THREADS; i++)
    {
        std::string ofile = "latency-";
        ofile.append(std::to_string(i));
        ofile.append(".txt");
        std::ofstream fs;
        fs.open(ofile);
        for (int v : l_records[i])
        {
            fs << v << std::endl;
        }
        fs.close();
    }

    return 0;
}
