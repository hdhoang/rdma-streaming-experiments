#pragma once

#include <amqp.h>
#include <amqp_tcp_socket.h>

#define NUM_TOPICS 10

int pubsub(int argc, char const *const *argv);
