#include <iostream>

#include "utils.hpp"
#include "latency.hpp"
#include "pubsub.hpp"

std::string ping_proc = "ping";
std::string pong_proc = "pong";
std::string pub_proc = "pub";
std::string sub_proc = "sub";

int main(int argc, char const *const *argv)
{
    if (argc >= 3)
    {
        if (ping_proc.compare(argv[1]) == 0 || pong_proc.compare(argv[1]) == 0)
        {
            return latency(argc, argv);
        }
        else if (pub_proc.compare(argv[1]) == 0 || sub_proc.compare(argv[1]) == 0)
        {
            return pubsub(argc, argv);
        }
    }

    std::cout << "rabbit [pub/sub-ping/pong] [broker_addr] [sub_id] ..." << std::endl;
    return EXIT_FAILURE;
}