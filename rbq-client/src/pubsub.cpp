#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <thread>
#include <iostream>
#include <fstream>
#include <atomic>
#include <chrono>
#include <vector>
#include <cstring>

#include <amqp.h>
#include <amqp_tcp_socket.h>
#include "utils.hpp"
#include "pubsub.hpp"

#define CHANNEL_SIZE (256 * 1024 * 2)

using namespace std::chrono_literals;

static std::atomic<int64_t> rates[NUM_TOPICS];
static char *MsgPayload;
static std::atomic<bool> running = false;
static size_t MsgSize = 8;
static std::vector<uint64_t> records;
static std::atomic<bool> recording = false;
static std::atomic<bool> sampling_stop = false;
static int sub_id = 0;

static std::chrono::seconds warmup = 3s;
static std::chrono::seconds main_period = 6s;

#define SEND_RATE 5
static void publisher_thread(const char *broker_addr, int id)
{
    printf("start publisher topic id %d payload size %zd\n", id, MsgSize);
    std::string exchange = std::to_string(id);
    amqp_connection_state_t conn = amqp_new_connection();
    amqp_socket_t *socket = amqp_tcp_socket_new(conn);
    if (!socket)
    {
        die("creating TCP socket");
    }
    int status = amqp_socket_open(socket, broker_addr, SERVER_PORT);
    if (status)
    {
        die("opening TCP socket");
    }

    die_on_amqp_error(amqp_login(conn, "/", 0, CHANNEL_SIZE, 0, AMQP_SASL_METHOD_PLAIN, "guest", "guest"),
                      "Logging in");
    amqp_channel_open(conn, 1);
    die_on_amqp_error(amqp_get_rpc_reply(conn), "Opening channel");

    // declare the exchange here
    amqp_exchange_declare(conn, 1, amqp_cstring_bytes(exchange.c_str()), amqp_cstring_bytes("fanout"), 0, 0, 0, 0, amqp_empty_table);
    die_on_amqp_error(amqp_get_rpc_reply(conn), "Declaring Exchange");
    printf("exchange declared\n");

    amqp_bytes_t message_bytes;
    message_bytes.bytes = MsgPayload;
    message_bytes.len = MsgSize;
    amqp_basic_properties_t props;
    props._flags = AMQP_BASIC_DELIVERY_MODE_FLAG;
    props.delivery_mode = 1;

    std::this_thread::sleep_for(1s * id);

    // auto start_point = std::chrono::high_resolution_clock::now();
    // int64_t sent = 0;

    //start sending
    while (running.load())
    {
        // auto period = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start_point);
        // int64_t expected_sent = period.count() * SEND_RATE;
        // if (sent > expected_sent)
        // {
        //     std::this_thread::sleep_for(1ms);
        //     continue;
        // }

        die_on_error(amqp_basic_publish(conn, 1, amqp_cstring_bytes(exchange.c_str()),
                                        amqp_empty_bytes, 0, 0,
                                        &props, message_bytes),
                     "Publishing");
        rates[id]++;
        // sent++;
    }
}

static void write_record(std::string &file_name)
{
    std::ofstream fs;
    fs.open(file_name);
    for (auto v : records)
    {
        fs << v << std::endl;
    }
    fs.flush();
    fs.close();
}

static void consumer_thread(const char *broker_addr, int id)
{
    printf("start consumer topic id %d on server %s\n", id, broker_addr);
    std::string exchange = std::to_string(id);
    amqp_connection_state_t conn = amqp_new_connection();
    amqp_socket_t *socket = amqp_tcp_socket_new(conn);
    if (!socket)
    {
        die("creating TCP socket");
    }

    int status = amqp_socket_open(socket, broker_addr, SERVER_PORT);
    if (status)
    {
        die("opening TCP socket");
    }

    die_on_amqp_error(amqp_login(conn, "/", 0, CHANNEL_SIZE, 0, AMQP_SASL_METHOD_PLAIN, "guest", "guest"),
                      "Logging in");
    amqp_channel_open(conn, 1);
    die_on_amqp_error(amqp_get_rpc_reply(conn), "Opening channel");

    // declare the exchange here
    amqp_exchange_declare(conn, 1, amqp_cstring_bytes(exchange.c_str()), amqp_cstring_bytes("fanout"), 0, 0, 0, 0, amqp_empty_table);
    die_on_amqp_error(amqp_get_rpc_reply(conn), "Declaring Exchange");
    printf("exchange declared\n");

    // so the way rabbitmq consumer works is that first we declare an "empty" queue
    amqp_queue_declare_ok_t *r = amqp_queue_declare(conn, 1, amqp_empty_bytes, 0, 0, 1, 1, amqp_empty_table);
    die_on_amqp_error(amqp_get_rpc_reply(conn), "Declaring queue");
    // then broker returns a generated queue name
    amqp_bytes_t queuename = amqp_bytes_malloc_dup(r->queue);
    // we bind this queue name to the exchange
    amqp_queue_bind(conn, 1, queuename, amqp_cstring_bytes(exchange.c_str()), amqp_empty_bytes, amqp_empty_table);
    die_on_amqp_error(amqp_get_rpc_reply(conn), "Binding queue");
    // then consume the queue
    amqp_basic_consume(conn, 1, queuename, amqp_empty_bytes,
                       0, 1, 0, amqp_empty_table);
    die_on_amqp_error(amqp_get_rpc_reply(conn), "Consuming");

    while (running.load())
    {
        amqp_rpc_reply_t res;
        amqp_envelope_t envelope;

        amqp_maybe_release_buffers(conn);

        res = amqp_consume_message(conn, &envelope, NULL, 0);

        if (AMQP_RESPONSE_NORMAL != res.reply_type)
        {
            break;
        }

        rates[id]++;
        amqp_destroy_envelope(&envelope);
    }

    die_on_amqp_error(amqp_channel_close(conn, 1, AMQP_REPLY_SUCCESS),
                      "Closing channel");
    die_on_amqp_error(amqp_connection_close(conn, AMQP_REPLY_SUCCESS),
                      "Closing connection");
    die_on_error(amqp_destroy_connection(conn), "Ending connection");
}

static void sampling_thread()
{
    for (int i = 0; i < NUM_TOPICS; i++)
    {
        rates[i] = 0;
    }

    uint64_t count = 0;
    while (running.load())
    {
        std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
        std::this_thread::sleep_for(2s);

        uint64_t new_count = 0;
        for (size_t i = 0; i < NUM_TOPICS; i++)
        {
            new_count += rates[i].load();
        }
        auto dur = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - start);
        uint64_t rate = (new_count - count) / dur.count();
        printf("rate %zd msg/s\n", rate / dur.count());
        if (recording.load())
        {
            records.push_back(rate);
        }
        count = new_count;
    }
}

int pubsub(int argc, char const *const *argv)
{
    std::vector<std::thread> runtime_threads;
    const char *broker_addr = argv[2];
    if (argc > 3)
    {
        MsgSize = std::atoi(argv[3]);
    }
    MsgPayload = (char *)malloc(MsgSize);

    if (argc > 4)
    {
        sub_id = std::atoi(argv[4]);
    }

    for (int i = 0; i < NUM_TOPICS; i++)
    {
        if (pub_proc.compare(argv[1]) == 0)
        {
            runtime_threads.push_back(std::thread(publisher_thread, broker_addr, i));
        }
        else
        {
            runtime_threads.push_back(std::thread(consumer_thread, broker_addr, i));
        }
    }
    std::string ofile;
    std::thread sampling = std::thread(sampling_thread);
    running = true;
    std::this_thread::sleep_for(warmup);
    recording = true;
    std::this_thread::sleep_for(main_period);
    recording = false;

    if (pub_proc.compare(argv[1]) == 0)
    {
        ofile = "rabbit_pub_" + std::to_string(MsgSize) + "B.txt";
        write_record(ofile);
        std::this_thread::sleep_for(5s);
    }
    running = false;

    for (int i = 0; i < NUM_TOPICS; i++)
    {
        runtime_threads[i].join();
    }
    sampling.join();
    if (sub_proc.compare(argv[1]) == 0)
    {
        ofile = "rabbit_sub" + std::to_string(sub_id) + "_" + std::to_string(MsgSize) + "B.txt";
        write_record(ofile);
        std::this_thread::sleep_for(5s);
    }

    return 0;
}
