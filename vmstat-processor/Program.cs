﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommandLine;
using System.Threading.Tasks;
using System.Threading;

namespace vmstat_processor
{
    // parse vmstat output series write to stdout
    class Program
    {
        public class ClOptions
        {
            [Option('t', "target", Required = true, HelpText = "Parse target in (name, data_file) format")]
            public IList<string> Targets { get; set; }

            [Option('c', "column", Required = false, HelpText = "Column index to get the data, default to 14 for vmstat", Default = 14)]
            public int ColumnIndex { get; set; }

            [Option('s', "start-cut", Required = false, HelpText = "portion of rows to cut off from the start", Default = 0.2)]
            public double StartCutoff { get; set; }

            [Option('e', "end-cut", Required = false, HelpText = "portion of rows to cut off from the end", Default = 0.1)]
            public double EndCutoff { get; set; }

            [Option('l', "", Required = false, HelpText = "read all text from file?", Default = false)]
            public bool ReadAllText { get; set; }

            [Option('d', "decimal", Required = false, HelpText = "output with decimal places?", Default = false)]
            public bool Decimal { get; set; }
        }

        const double z95 = 1.960;

        // return mean and confidence interval
        public static (double, double) Process(IEnumerable<int> values)
        {
            // Get the mean.
            double mean = (values.Sum() * 1.0) / values.Count();
            int meanInt = (int)mean;

            // Get the sum of the squares of the differences
            // between the values and the mean.
            var squares =
                from int value in values
                select (value - mean) * (value - mean);
            double sumOfSquares = squares.Sum();
            double stdDev = Math.Sqrt(sumOfSquares / values.Count());
            double confItv = z95 * (stdDev / Math.Sqrt(values.Count()));

            return (mean, confItv);
        }

        private static (string, double, double) ParseSeries(string name, TextReader file, ClOptions opts)
        {
            var values = new List<int>();
            while (file.Peek() >= 0)
            {
                var line = file.ReadLine()?.Trim();
                if (line == null)
                {
                    break;
                }

                var columns = line.Split(' ', StringSplitOptions.RemoveEmptyEntries);
                if (columns.Length == 0 || columns[0].Length == 0 || !char.IsNumber(columns[0][0]))
                {
                    continue;
                }

                if (columns.Length <= opts.ColumnIndex)
                {
                    // something wrong happened here 
                    throw new Exception($"columns.Length <= TargetColumn ({columns.Length},{opts.ColumnIndex})");
                }

                if (!int.TryParse(columns[opts.ColumnIndex], out var num))
                {
                    throw new Exception($"Invalid value {columns[opts.ColumnIndex]}");
                }

                values.Add(num);
            }

            // make sure we have at least 100 values to work with 
            if (values.Count < 30)
            {
                throw new Exception($"too few data points {values.Count}");
            }

            //remove the top and last 10%
            int headCut = (int)(opts.StartCutoff * values.Count);
            int endCut = (int)(opts.EndCutoff * values.Count);

            if (headCut + endCut > values.Count)
            {
                throw new Exception($"cut off to many");
            }

            for (int i = 0; i < headCut; i++)
            {
                values.RemoveAt(0);
            }

            for (int i = 0; i < endCut; i++)
            {
                values.RemoveAt(values.Count - 1);
            }

            var (mean, confItv) = Process(values);
            return (name, mean, confItv);
        }


        public static void Main(string[] args)
        {
            Parser.Default.ParseArguments<ClOptions>(args)
            .WithParsed<ClOptions>(o =>
            {
                int wt, iot;
                ThreadPool.GetMinThreads(out wt, out iot);
                ThreadPool.SetMinThreads(o.Targets.Count, iot);

                var tasks = o.Targets.Select(t => Task.Run(() =>
                {
                    var kvp = t.Split(',', StringSplitOptions.RemoveEmptyEntries);
                    if (kvp.Length != 2)
                    {
                        throw new Exception($"invalid target string {t}");
                    }

                    // var file = File.Open(kvp[1], FileMode.Open, FileAccess.Read);

                    // this is horrible but the dotnet sdk sucks with NFS so just read it in one go

                    if (o.ReadAllText)
                    {
                        var text = File.ReadAllText(kvp[1]);
                        using (var ss = new StringReader(text))
                        {
                            return ParseSeries(kvp[0], ss, o);
                        }
                    }


                    using (FileStream fs = File.OpenRead(kvp[1]))
                    {
                        using (var sr = new StreamReader(fs))
                        {
                            return ParseSeries(kvp[0], sr, o);
                        }
                    }

                    // file.Close();
                }));

                var tasksAll = Task.WhenAll(tasks);
                tasksAll.Wait();

                foreach (var result in tasksAll.Result)
                {
                    // write to stdout
                    if (o.Decimal)
                    {
                        Console.WriteLine($"{result.Item1} {result.Item2:N2} {result.Item3:N2}");
                    }
                    else
                    {
                        Console.WriteLine($"{result.Item1} {(int)result.Item2:N2} {(int)result.Item3:N2}");
                    }
                }
            });
        }
    }
}
