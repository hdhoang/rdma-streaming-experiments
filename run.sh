#!/bin/bash
cur_dir=$(pwd);

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$cur_dir/bin && \
./bin/userver-live \
--dont-exit-on-not-found \
--stream-config=/home/hdhoang/kappa_conf/kappa_red02-03.cfg \
--max-conns=1000 --max-fds=2000 \
--ignore-fd-setsize \
--ssl-port=6443 \
--port=6800 \
--use-epoll \
--reply-buffer-size=256 \
--read-buffer-size=256 \
