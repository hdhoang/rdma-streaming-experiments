nums_physical_cpu=$(cat /proc/cpuinfo | grep "physical id" | sort | uniq | wc -l)
num_physical_cores=$(cat /proc/cpuinfo | grep "cpu cores" | head -n1)
num_physical_cores=$(echo $num_physical_cores | sed 's/^cpu cores ://')
num_virtual_cores=$(nproc --all);
stops=0;

reset() {
    let end=$num_virtual_cores-1;
    for i in $(seq 1 "$end");
    do
        # this will cause some error if num_cores<23 but nothing harmful
        echo 1 | sudo tee /sys/devices/system/cpu/cpu$i/online;
    done
}

start() {
    let end=$num_virtual_cores-1;
    for i in $(seq 1 "$end");
    do
        if [ "$i" -ge "$1" ]; then
            echo "Disabling cpu$i";
            echo 0 | sudo tee /sys/devices/system/cpu/cpu$i/online;
        else
            echo "Enabling cpu$i";
            echo 1 | sudo tee /sys/devices/system/cpu/cpu$i/online;
        fi
    done
}

aqua00() {
    for i in $(seq 1 9);
    do
        echo "Enabling cpu$i";
        echo 1 | sudo tee /sys/devices/system/cpu/cpu$i/online;
    done
    
    for i in $(seq 10 19);
    do
        echo "Disabling cpu$i";
        echo 0 | sudo tee /sys/devices/system/cpu/cpu$i/online;
    done
    
    for i in $(seq 20 29);
    do
        echo "Enabling cpu$i";
        echo 1 | sudo tee /sys/devices/system/cpu/cpu$i/online;
    done
    
    for i in $(seq 30 39);
    do
        echo "Disabling cpu$i";
        echo 0 | sudo tee /sys/devices/system/cpu/cpu$i/online;
    done
}

case "$1" in
    start)
        stops=$2;
        echo "Enabling" $stops "CPUs";
        start $stops;
        echo "done";
    ;;
    
    reset)
        echo "Enabling  all" "$num_virtual_cores" "CPUs";
        reset;
        echo "done"
    ;;
    
    count)
        echo "physical CPUs:" $nums_physical_cpu;
        echo "physical cores:" $num_physical_cores;
        echo "virtual cores:" $num_virtual_cores;
    ;;
    
    aqua00)
        aqua00;
    ;;
esac
exit 0;
